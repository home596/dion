<?php

namespace dion\testmodule;


use yii\base\Application;

class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'dion\testmodule\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        $this->controllerNamespace = __NAMESPACE__ . '\controllers';
        $this->addMigrationNamespace($app);
    }

    private function addMigrationNamespace(Application $app)
    {
        if (!isset($app->controllerMap['migrate'])) {
            $app->controllerMap['migrate'] = [
                'class' => \yii\console\controllers\MigrateController::class,
            ];
        }
        if (!isset($app->controllerMap['migrate']['migrationPath'])) {
            $app->controllerMap['migrate']['migrationPath'] = ['@app/migrations'];
        }
        $app->controllerMap['migrate']['migrationNamespaces'][] = __NAMESPACE__ . '\migrations';
    }


}
