<?php

use yii\db\Connection;

return [
    'class' => Connection::class,
    /** Тут host=percona - это контейнер с percona */
    'dsn' => 'mysql:host=percona;dbname=dion;',
    'username' => 'dion',
    'password' => '1ecbd865ebd170d70256681ddf24a31133ff77',
    'charset' => 'utf8mb4',
];
