<?php

use yii\db\Connection;

return [
    'class' => Connection::class,
    /** Тут host=percona - это контейнер с percona */
    'dsn' => 'mysql:host=dion-percona;dbname=dion;',
    'username' => 'dion',
    'password' => 'dion',
    'charset' => 'utf8mb4',
];
