ifeq ($(STAGE),)
STAGE = local-dev
endif

OPT =
.PHONY: build clean

COMPOSE = docker-compose -p dion \
	-f ./docker/env/${STAGE}/docker-compose.yml \
	--project-directory ./docker/env/${STAGE}

RUN_IN_PHP = docker exec -i dion-php-fpm
RUN_IN_PERCONA = docker exec -i dion-percona
RUN_IN_NGINX = docker exec -i dion-nginx

up:
	${COMPOSE} up -d

setup:
	php ./docker/env/${STAGE}/setup.php

build:
	make composer
	make setup
	make migrate

composer:
	${RUN_IN_PHP} composer update --prefer-dist

migrate:
	make flush-schema-cache
	${RUN_IN_PHP} php /var/www/yii migrate --interactive=0

migrate-down:
	make flush-schema-cache
	${RUN_IN_PHP} php /var/www/yii migrate/down ${AMOUNT} --interactive=0

migrate-create:
	${RUN_IN_PHP} php /var/www/yii migrate/create ${NAME} --interactive=0

restart:
	make down
	make up

down:
	${COMPOSE} down --remove-orphans

flush-schema-cache:
	${RUN_IN_PHP} php /var/www/yii cache/flush-schema --interactive=0

clean:
	make down
	docker volume rm dion-data dion-logs dion_nginx-logs